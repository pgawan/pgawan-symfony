<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=24)
     * @Assert\Length(
     *      min = 8,
     *      max = 24,
     *      minMessage = "Nazwa uzytkownika musi miec wiecej {{ limit }} znaki",
     *      maxMessage = "Nazwa użytkownika nie może mieć wiecej niż  {{ limit }} znaków"
     * )
     */
    private $Username;

    /**
     * @ORM\Column(type="string", length=24)
     * @Assert\Length(
     *      min = 8,
     *      max = 24,
     *      minMessage = "Hasło musi miec wiecej {{ limit }} znaki",
     *      maxMessage = "Hasło nie może mieć wiecej niż  {{ limit }} znaków"
     * )
     */
    private $Password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Email;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->Username;
    }

    public function setUsername(string $Username): self
    {
        $this->Username = $Username;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->Password;
    }

    public function setPassword(string $Password): self
    {
        $this->Password = $Password;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->Email;
    }

    public function setEmail(string $Email): self
    {
        $this->Email = $Email;

        return $this;
    }
}
