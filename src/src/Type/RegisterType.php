<?php
/**
 * Created by PhpStorm.
 * User: pgkek
 * Date: 21.03.19
 * Time: 14:46
 */

namespace App\Type;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Validator\Constraints\Length;



class RegisterType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('Username', TextType::class,[
            'mapped' => false,
            'constraints' => [
                new Length([
                    'min' => 5,
                    'minMessage' => 'Min 5',
                    'max' => 16,
                    'maxMessage' => 'max 16'
                ]),
            ],
        ])
        ->add('Password', PasswordType::class)
        ->add('Email', EmailType::class)
        ->add('Register', SubmitType::class, ['label' => 'Zarejestruj sie'])
            ->getForm();
    ;
    }









}