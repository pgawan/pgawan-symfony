<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\Session;

class SecretController extends AbstractController
{
    public function Secret()
    {
        $session = new Session();

        if ($session->get('isLogged' ) == true) {
            return $this->render('main/Secret.html.twig');
        }

        return $this->render('main/nopermission.html.twig');
    }
}