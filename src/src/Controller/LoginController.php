<?php
/**
 * Created by PhpStorm.
 * User: pgkek
 * Date: 14.03.19
 * Time: 16:25
 */

namespace App\Controller;


use App\Entity\User;
use App\Type\LoginType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;


class LoginController extends AbstractController
{
    public function login(Request $request,$slug)
    {
        $user = new User();
        $form = $this->createForm(LoginType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted()){
            $username = $form->get('username')->getData();
            $password = $form->get('password')->getData();

            $repository = $this->getDoctrine()->getRepository(User::class);
            $loginuser = $repository->findOneBy([
                'Username' => $username,
                'Password' => $password,
            ]);

            if($loginuser == true) {
                $session = new Session();
                $session ->set('isLogged', true);
                return $this->redirectToRoute('login_success', [
                    'slug' => $slug,

                ]);

            } else {
                echo "błędne dane";
            }
    }

        return $this->render('main/login.html.twig',[
            'form' => $form->createView(),
        ]);

}
    public function LoginSuccess (Request $request) {
        return $this->render('main/login_success.html.twig');
    }
}