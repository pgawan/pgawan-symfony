<?php
/**
 * Created by PhpStorm.
 * User: pgkek
 * Date: 07.03.19
 * Time: 15:35
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\Session;

class MainController extends AbstractController
{



    public function index($slug = "Nieznajomy")
    {

            return $this->render('main/index.html.twig', ['slug' => $slug]);


    }





}