<?php
/**
 * Created by PhpStorm.
 * User: pgkek
 * Date: 07.03.19
 * Time: 14:55
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LuckyController extends AbstractController
{
//    /**
//     * @Route("lucky/number")
//     */
//    public function number()
//    {
//
//        $number = random_int(0, 100);
//
//        return new Response(
//            '<html><body>Lucky number: '.$number.'</body></html>'
//        );
//    }
    /**
     * @Route("lucky/number2")
     */
    public function number2()
    {

        {
            $number = random_int(0, 100);

            return $this->render('lucky/number.html.twig', [
                'number' => $number,
            ]);
        }
    }



}