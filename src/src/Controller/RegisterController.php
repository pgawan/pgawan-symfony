<?php
/**
 * Created by PhpStorm.
 * User: pgkek
 * Date: 14.03.19
 * Time: 16:54
 */

namespace App\Controller;

use App\Entity\User;
use App\Type\RegisterType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;


class RegisterController extends AbstractController
{
    public function register(Request $request)
    {
        $user = new User();
        $form = $this->createForm(RegisterType::class, $user);

        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            $user->setUsername(
                $form->get('Username')->getData()
            );
            $user->setPassword(
                $form->get('Password')->getData()
            );
            $user->setEmail(
                $form->get('Email')->getData()
            );



            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            return $this->redirectToRoute('register_success');
        }


        return $this->render('main/register.html.twig', [
            'form' => $form->createView()
        ]);
    }

        public function registerSuccess (Request $request) {
            return $this->render('main/register_success.html.twig');
    }
        }